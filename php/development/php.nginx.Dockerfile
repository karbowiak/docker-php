ARG PHP_VERSION

FROM karbowiak/docker-php:php${PHP_VERSION}

# Install Supervisor
COPY --from=ochinchina/supervisord:latest /usr/local/bin/supervisord /usr/local/bin/supervisord
COPY php/development/supervisor/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

ARG PHP_VERSION

# Switch working directory
WORKDIR /var/www

ARG PHP_VERSION

# Copy FPM Config
COPY php/development/fpm/www.conf /etc/php/${PHP_VERSION}/fpm/pool.d/www.conf
COPY php/development/fpm/php-fpm.conf /etc/php/${PHP_VERSION}/fpm/php-fpm.conf
COPY php/development/fpm/www-xdebug.conf /etc/php/${PHP_VERSION}/fpm/pool.d/www-xdebug.conf
COPY php/development/fpm/php-fpm-xdebug.conf /etc/php/${PHP_VERSION}/fpm/php-fpm-xdebug.conf

# Copy nginx executable
COPY php/development/nginx/nginx-server.sh /usr/local/bin/nginx-server

RUN apt update && \
    apt install -o DPkg::Options::="--force-confnew" -y --no-install-recommends nginx-full mysql-client && \
    echo "include=/etc/php/${PHP_VERSION}/fpm/pool.d/www.conf" >> /etc/php/${PHP_VERSION}/fpm/php-fpm.conf && \
    echo "include=/etc/php/${PHP_VERSION}/fpm/pool.d/www-xdebug.conf" >> /etc/php/${PHP_VERSION}/fpm/php-fpm-xdebug.conf && \
    chmod +x /usr/local/bin/nginx-server && \
    echo "#!/bin/bash" > /usr/local/bin/fpm && \
    echo "mkdir -p /run/php" >>/usr/local/bin/fpm && \
    echo "/usr/sbin/php-fpm${PHP_VERSION} -R -F -y /etc/php/${PHP_VERSION}/fpm/php-fpm.conf" >> /usr/local/bin/fpm && \
    chmod +x /usr/local/bin/fpm && \
    echo "#!/bin/bash" > /usr/local/bin/fpm-xdebug && \
    echo "mkdir -p /run/php" >> /usr/local/bin/fpm-xdebug && \
    echo "/usr/sbin/php-fpm${PHP_VERSION} -R -F -y /etc/php/${PHP_VERSION}/fpm/php-fpm-xdebug.conf -dzend_extension=xdebug.so" >> /usr/local/bin/fpm-xdebug && \
    chmod +x /usr/local/bin/fpm-xdebug && \
    apt clean && apt autoclean -y && apt autoremove -y && rm -rf /var/lib/{apt,dpkg,cache,log}/

# Copy nginx config
COPY php/development/nginx/nginx.conf /etc/nginx/nginx.conf
COPY php/development/nginx/www.conf.tmpl /etc/nginx/conf.d/www.conf.tmpl
